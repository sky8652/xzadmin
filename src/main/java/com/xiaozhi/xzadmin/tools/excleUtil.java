package com.xiaozhi.xzadmin.tools;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.extra.compress.CompressUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * excle地址类
 *
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/4/8 23:35
 */
@Slf4j
public class excleUtil {
    public static void creatExcleXls(String path, String name) throws IOException {
//        String path = "D:/home/";
        //1.创建工作簿
        Workbook workbook = new HSSFWorkbook();
        //2.创建表名
        Sheet sheet = workbook.createSheet("sheet1");
        //3.创建流用于输出
        FileOutputStream fileOutputStream = new FileOutputStream(path + name+".xls");
        //4.输出
        workbook.write(fileOutputStream);
        fileOutputStream.close();
    }
    public static void creatExcleXlsx(String path, String name) throws IOException {
//        String path = "D:/home/";
        //1.创建工作簿
        Workbook workbook = new XSSFWorkbook();
        //2.创建表名
        Sheet sheet = workbook.createSheet("sheet1");
        //3.创建流用于输出
        FileOutputStream fileOutputStream = new FileOutputStream(path + name+".xlsx");
        //4.输出
        workbook.write(fileOutputStream);
        fileOutputStream.close();
    }

    /***
     * 文件合并xlx
     * path 保存路径
     * writepath 需要合并文件的路径
     * @return
     */
    public static void mergeExcleXls(String path,String writepath) throws IOException {
//        String path = "D:/home/";
//        String name = "test.xls";
        //当前日期字符串，格式：yyyy-MM-dd
        String today = DateUtil.today();
        excleUtil.creatExcleXls(path, today);

//        String filepath = "D:/home/贵阳1/";
        File[] files = FileUtil.ls(writepath);
        for (File file : files) {
            String fileName = file.getName();
            //                文件类型
            String filetype = FilenameUtils.getExtension(fileName).toLowerCase();
            if (filetype.equals("xls") || filetype.equals("xlsx")) {
                ExcelReader reader = ExcelUtil.getReader(writepath + fileName);
                List<Map<String, Object>> readAll = reader.readAll();

                excleUtil.creatExcleXls(path,today);
                ExcelWriter writer = ExcelUtil.getWriter(path + today + ".xls");
                // 合并单元格后的标题行，使用默认标题样式
//                writer.merge(row1.size() - 1, "一班成绩单");
                // 一次性写出内容，使用默认样式，强制输出标题
                writer.write(readAll);
                // 关闭writer，释放内存
                writer.close();
            }
        }
        log.info("文件写入完成");
    }
    /***
     * 文件合并xlxs
     * path 保存路径
     * writepath 需要合并文件的路径
     * @return
     */
    public static void mergeExcleXlsx(String path,String writepath) throws IOException {
//        String path = "D:/home/";
//        String name = "test.xls";
        //当前日期字符串，格式：yyyy-MM-dd
        String today = DateUtil.today();
        excleUtil.creatExcleXlsx(path, today);

//        String filepath = "D:/home/贵阳1/";
        File[] files = FileUtil.ls(writepath);
        for (File file : files) {
            String fileName = file.getName();
            //                文件类型
            String filetype = FilenameUtils.getExtension(fileName).toLowerCase();
            if (filetype.equals("xls") || filetype.equals("xlsx")) {
                ExcelReader reader = ExcelUtil.getReader(writepath + fileName);
                List<Map<String, Object>> readAll = reader.readAll();

                excleUtil.creatExcleXls(path,today);
                ExcelWriter writer = ExcelUtil.getWriter(path + today + ".xlsx");
                // 合并单元格后的标题行，使用默认标题样式
//                writer.merge(row1.size() - 1, "一班成绩单");
                // 一次性写出内容，使用默认样式，强制输出标题
                writer.write(readAll);
                // 关闭writer，释放内存
                writer.close();
            }
        }
        log.info("文件写入完成");
    }

    /***
     * 文件拆分xlxs
     * numbers 拆分份数
     * path 拆分路径
     * writepath 拆分文件保存文件夹
     * @return
     */
    public static void splitExcleXls(String path,String splitName,int numbers,String wiritepath) throws IOException {
//        int numbers = 5;
//        String path = "D:/home/";
//        String wiritepath = "D:/home/excle/1/";
        FileUtil.del(wiritepath);
        FileUtil.mkdir(wiritepath);
        //当前日期字符串，格式：yyyy-MM-dd
//        String today = DateUtil.today();
        ExcelReader reader = ExcelUtil.getReader(path+splitName+".xls");
        List<Map<String,Object>> readAll = reader.readAll();

        int number1 = readAll.size() / numbers;
        System.out.println(number1);
        for (int i = 0; i < numbers; i++) {
            //创建excle文件
            excleUtil.creatExcleXls(wiritepath,splitName+"-"+i);
            //通过工具类创建writer
            ExcelWriter writer = ExcelUtil.getWriter(wiritepath+splitName+"-"+i+".xls");

            int start = number1*i;
            int end = number1*(i+1);
            if (i ==(numbers-1)){
                end = readAll.size();
            }
            //一次性写出内容，强制输出标题
            writer.write(readAll.subList(start,end));
            //关闭writer，释放内存
            writer.close();
        }
    }
    /***
     * 创建zip
     * path 路径
     * zipName zip名称
     * @return
     */
    public static void createZip(String path, String zipName){
//        path = "D:/home/excle/1/"
        final File file = FileUtil.file(path+zipName+".zip");
        CompressUtil.createArchiver(CharsetUtil.CHARSET_UTF_8, ArchiveStreamFactory.ZIP, file)
                .add(FileUtil.file(path), (file1)->{
                    System.out.println(file1.getName());
                    if((zipName+".zip").equals(file1.getName())){
                        return false;
                    }
                    return true;
                })
                .finish()
                .close();
    }
}
