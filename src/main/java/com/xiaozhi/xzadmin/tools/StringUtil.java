package com.xiaozhi.xzadmin.tools;

import com.xiaozhi.xzadmin.pojo.UserToRole;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * String工具类
 *
 * @author xiaozhi
 */
public class StringUtil {
    public static Boolean isNotEmpty(Object msg){
        if (String.valueOf(msg).isEmpty()) {
            return true;
        }
        return false;
    }

//    删除id裁剪
    public static ArrayList<String> cutString(String msg){
        ArrayList<String> strings = new ArrayList<>();
        if (msg.length() !=1){
            String[] split = msg.split(",");
            strings.addAll(Arrays.asList(split));
        }else {
            strings.add(msg);
        }

        return strings;
    }

//    userToRoles List<UserToRole>裁剪
    public static ArrayList<Integer> cutList(List<UserToRole> userToRoles){
        ArrayList<Integer> strings = new ArrayList<>();
        if (userToRoles.size() != 0){
            for (int i = 0; i < userToRoles.size(); i++) {
                strings.add(userToRoles.get(i).getUid());
            }
            return  strings;
        }

        return strings;
    }
    //计算字符串在给定字符串出现的次数
    public static int findCount(String src,String des) {
        int index = 0;
        int count = 0;
        while((index = src.indexOf(des, index)) != -1) {
            count++;
            index = index + des.length();
        }
        return count;
    }
}
