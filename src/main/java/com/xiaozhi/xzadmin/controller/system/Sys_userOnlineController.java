package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserOnline;
import com.xiaozhi.xzadmin.service.sys.UserOnlineService;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/5/10 23:01
 */
@Controller
@RequestMapping("/system/useronline")
@SaCheckLogin
public class Sys_userOnlineController extends BaseController {
    @Autowired
    private UserOnlineService userOnlineService;

    @AopUserOnline
    @AopLogger(title = "用户在线管理", type = 0)
    @SaCheckPermission("useronline:view")
    @RequestMapping({"/", ""})
    public String useronlinehome() {
        return "/system/useronline/useronline";
    }

    /***
     * 用户查询
     * @page 页码
     * @limit 页码长度
     * @username 用户名
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户在线管理", type = 0)
    @SaCheckPermission("useronline:view:select")
    @RequestMapping("/useronlinequery")
    @ResponseBody
    public AjaxResult useronlinequery(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                      @RequestParam(value = "limit", required = false, defaultValue = "5") Integer limit,
                                      @RequestParam(value = "username", required = false) String username,
                                      @RequestParam(value = "ipaddr", required = false) String ipaddr) {
        Page<UserOnline> userOnlinePage = userOnlineService.getUserOnlinePage(page, limit, username, ipaddr);
        return success(userOnlinePage);
    }

    /***
     * 用户在线强退
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户在线管理", type = 3)
    @SaCheckPermission("useronline:view:delete")
    @RequestMapping("/remove")
    @ResponseBody
    public AjaxResult removeUser(@RequestBody Map<String, String> map) {
        String sessionId = map.get("sessionId");
        ArrayList<String> sessionIds = StringUtil.cutString(sessionId);
        for (String id : sessionIds) {
            UserOnline userOnline = new UserOnline();
            userOnline.setStatus("off_line");
            //token
            userOnline.setSessionId(id);
            userOnline.setLastAccessTime(new Date());
            userOnlineService.update(userOnline);

            StpUtil.kickoutByTokenValue(id);
        }
        return success("强退成功");
    }
}
