package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.service.sys.LogininforService;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 23:01
 */
@Controller
@RequestMapping("/system/logininfor")
@SaCheckLogin
public class Sys_logininforController extends BaseController {

    @Autowired
    private LogininforService logininforService;

    /***
     * 登录日志管理页
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "登录日志",type = 0)
    @RequestMapping({"/",""})
    @SaCheckPermission("logininfor:view")
    public String home(){
        return "/system/logininfor/logininfor";
    }


    /***
     * 登录日志查询
     * @page 页码
     * @limit 页码长度
     * @logininfor logininfor实体类
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "登录日志",type = 0)
    @SaCheckPermission("logininfor:view")
    @RequestMapping("/logininforquery")
    @ResponseBody
    public AjaxResult logininforquery(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                          @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                          Logininfor logininfor){
        Page<Logininfor> userPage = logininforService.getLogininforPage(page,limit,logininfor);
        return success(userPage);
    }

}
