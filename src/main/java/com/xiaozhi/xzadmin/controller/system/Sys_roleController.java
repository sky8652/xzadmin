package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.service.sys.RoleService;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 23:01
 */
@Controller
@RequestMapping("/system/role")
@SaCheckLogin
public class Sys_roleController extends BaseController {

    @Autowired
    private RoleService roleService;

    /***
     * 授权页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 0)
    @SaCheckPermission("role:view")
    @RequestMapping({"",})
    public String rolehome(){
        return "/system/role/role";
    }


    /***
     * 角色查询
     * @page 页码
     * @limit 页码长度
     * @username 角色名
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 0)
    @SaCheckPermission("role:view:select")
    @RequestMapping("/rolequery")
    @ResponseBody
    public AjaxResult api(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                          @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                          @RequestParam(value = "roleName", required = false)String roleName
                          ){
        Page<Role> userPage = roleService.getRolePage(page,limit,roleName);
        return success(userPage);
    }

    /***
     * 封禁设置
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 5)
    @SaCheckPermission("role:view:banned")
    @RequestMapping("/openupdate")
    @ResponseBody
    public AjaxResult openupdate(@RequestBody Role role) {
        Boolean flag = roleService.isOpenupdateByid(role);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }
    /***
     * 角色添加页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 1)
    @SaCheckPermission("role:view:add")
    @GetMapping({"/add","/add.html"})
    public String roleadd(){
        return "/system/role/add";
    }
    /***
     * 角色添加
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 1)
    @SaCheckPermission("role:view:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult insertRole(@RequestBody Role role) {
        if ("".equals(role.getRoleName()) || role.getRoleName()==null){
            return AjaxResult.error("角色名不能为空");
        }
        if (roleService.selectRoleByname(role)){
            return AjaxResult.error("角色名已存在");
        }
        Boolean flag = roleService.insetRole(role);
        if (flag){
            return success("添加成功");
        }
        return error("添加失败");
    }
    /***
     * 角色修改页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 2)
    @SaCheckPermission("role:view:edit")
    @RequestMapping({"/edit","/edit.html"})
    public String roleedit(Integer roleId, Model model){
        Role role = roleService.getRoleByid(roleId);
        model.addAttribute("role",role);
        return "/system/role/edit";
    }
    /***
     * 角色修改
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 2)
    @SaCheckPermission("role:view:edit")
    @RequestMapping("/save")
    @ResponseBody
    public AjaxResult saveRole(@RequestBody Role role) {
        Boolean flag = roleService.updateRoleByid(role);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }

    /***
     * 角色删除
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 3)
    @SaCheckPermission("role:view:delete")
    @RequestMapping("/remove")
    @ResponseBody
    public AjaxResult removeRole(@RequestBody Map<String,String> map) {
        String userid = map.get("roleId");
        ArrayList<String> strings = StringUtil.cutString(userid);

        Boolean flag = roleService.delRoleByid(strings);
        if (flag){
            return success("删除成功");
        }
        return error("删除失败");
    }
    /***
     * 角色授权修改页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 2)
    @SaCheckPermission("role:view:authorization:edit")
    @GetMapping({"/getrolechange","/getrolechange.html"})
    public String getrolechange(Integer rid,Model model){
        model.addAttribute("rid",rid);
        return "/system/role/getrolechange";
    }
    /***
     * 角色授权查询接口
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 0)
    @SaCheckPermission("role:view:authorization:select")
    @GetMapping({"/rolechange","/rolechange.html"})
    @ResponseBody
    public AjaxResult rolechange(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                             @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                             @RequestParam(value = "rid", required = false) Integer rid,
                             @RequestParam(value = "username", required = false)String username,
                             @RequestParam(value = "phone", required = false)Integer phone){

        if (rid ==null){
            return error("参数异常");
        }
        if (!roleService.queryUserByRoleidCount(rid)){
            return success("数据为空");
        }
        Page<User> userPage = roleService.getUserByRoleid(page,limit,rid,username,phone);
        if (userPage.getRecords().size() ==0){
            return error("数据为空");
        }
        return success(userPage);
    }

    /***
     * 取消角色授权
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 3)
    @SaCheckPermission("role:view:authorization:delete")
    @RequestMapping("/removeUserRole")
    @ResponseBody
    public AjaxResult removeUserRole(@RequestBody Map<String,String> map) {
        String userid = map.get("userid");
        String rid = map.get("rid");
        if (userid == null || userid.trim().length() == 0){
            return error("参数异常");
        }
        if (rid == null || rid.trim().length() == 0){
            return error("参数异常");
        }
        ArrayList<String> strings = StringUtil.cutString(userid);

        Boolean flag = roleService.delRoleToUserByid(strings,rid);
        if (flag){
            return success("取消授权成功");
        }
        return error("取消授权失败");
    }
    /***
     * 角色授权添加页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 1)
    @SaCheckPermission("role:view:authorization:add")
    @GetMapping({"/rolechangeadd","/rolechangeadd.html"})
    public String rolechangeadd(Integer rid,Model model){
        model.addAttribute("rid",rid);
        return "/system/role/rolechange";
    }
    /***
     * 角色未授权查询接口
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 0)
    @SaCheckPermission("role:view:noauthorization:select")
    @GetMapping({"/norolechange","/norolechange.html"})
    @ResponseBody
    public AjaxResult norolechange(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                                 @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                                 @RequestParam(value = "username", required = false)String username,
                                 @RequestParam(value = "phone", required = false)Integer phone){

        Page<User> userPage = roleService.getNotUserByRoleid(page,limit,username,phone);
        if (userPage.getRecords().size() ==0){
            return error("数据为空");
        }
        return success(userPage);
    }

    /***
     * 添加未角色授权
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "角色管理",type = 1)
    @SaCheckPermission("role:view:noauthorization:add")
    @RequestMapping("/addUserRole")
    @ResponseBody
    public AjaxResult addUserRole(@RequestBody Map<String,String> map) {
        String userid = map.get("userid");
        String rid = map.get("rid");
        if (userid == null || userid.trim().length() == 0){
            return error("参数异常");
        }
        if (rid == null || rid.trim().length() == 0){
            return error("参数异常");
        }
        ArrayList<String> strings = StringUtil.cutString(userid);

        Boolean flag = roleService.addRoleToUserByid(strings,rid);
        if (flag){
            return success("授权成功");
        }
        return error("授权失败");
    }
}
