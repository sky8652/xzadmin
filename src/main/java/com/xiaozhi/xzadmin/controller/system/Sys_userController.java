package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 23:01
 */
@Controller
@RequestMapping("/system")
@SaCheckLogin
public class Sys_userController extends BaseController {
    @Autowired
    private UserService userService;


    @AopUserOnline
    @AopLogger(title = "用户管理",type = 0)
    @SaCheckPermission("user:view")
    @RequestMapping({"/user","/user.html"})
    public String home(){
        return "/system/user/user";
    }


    @AopUserOnline
    @AopLogger(title = "用户管理",type = 0)
    @SaCheckPermission("user:view")
    @RequestMapping({"/user/person","/user/person.html"})
    public String homeperson(){
        return "/system/user/person";
    }
    /***
     * 用户查询
     * @page 页码
     * @limit 页码长度
     * @username 用户名
     * @email 邮箱
     * @sex 性别
     * @return
     */
    @AopUserOnline
    @SaCheckPermission("user:view:select")
    @RequestMapping("/user/userquery")
    @ResponseBody
    public AjaxResult api(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                          @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                          @RequestParam(value = "username", required = false)String username,
                          @RequestParam(value = "email", required = false)String email,
                          @RequestParam(value = "sex", required = false)Integer sex){
        Page<User> userPage = userService.getUserPage(page,limit,username,email,sex);
        return success(userPage);
    }
    /***
     * 封禁设置
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 5)
    @SaCheckPermission("user:view:banned")
    @RequestMapping("/user/offupdate")
    @ResponseBody
    public AjaxResult offupdate(@RequestBody User user) {
        if (user.getIs_off()==1){
            // 先踢下线
            StpUtil.kickout(user.getUserId());
            // 再封禁账号
            StpUtil.disable(user.getUserId(), 86400);
        }else {
            // 解除封禁
            StpUtil.untieDisable(user.getUserId());
        }


        Boolean flag = userService.isOffupdateByid(user);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }
    /***
     * 用户信息添加页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 1)
    @SaCheckPermission("user:view:add")
    @GetMapping({"/user/add","/user/add.html"})
    public String useradd(){
        return "/system/user/add";
    }
    /***
     * 用户信息添加
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 1)
    @SaCheckPermission("user:view:add")
    @PostMapping("/user/add")
    @ResponseBody
    public AjaxResult insertUser(@RequestBody User user) {
        if ("".equals(user.getUsername()) || user.getUsername()==null){
            return AjaxResult.error("用户名不能为空");
        }
        if (userService.selectUserByusername(user)){
            return AjaxResult.error("用户名已存在");
        }
        Boolean flag = userService.insetUser(user);
        if (flag){
            return success("添加成功");
        }
        return error("添加失败");
    }
    /***
     * 用户信息修改页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 1)
    @SaCheckPermission("user:view:edit")
    @RequestMapping({"/user/useredit","/user/edit.html"})
    public String useredit(Integer userId, Model model){
        User user = userService.getUserByid(userId);
        model.addAttribute("user",user);
        return "/system/user/edit";
    }
    /***
     * 用户信息修改
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 2)
    @SaCheckPermission("user:view:edit")
    @RequestMapping("/user/save")
    @ResponseBody
    public AjaxResult saveUser(@RequestBody User user) {
        Boolean flag = userService.updateUserByid(user);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }

    /***
     * 用户信息删除
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 3)
    @SaCheckPermission("user:view:delete")
    @RequestMapping("/user/remove")
    @ResponseBody
    public AjaxResult removeUser(@RequestBody Map<String,String> map) {
        String userid = map.get("userid");
        ArrayList<String> strings = StringUtil.cutString(userid);

        Boolean flag = userService.delUserByid(strings);
        if (flag){
            return success("删除成功");
        }
        return error("删除失败");
    }

    /***
     * 用户密码页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 3)
    @SaCheckPermission("user:view:pwdmodify")
    @GetMapping({"/user/changepwd","/user/changepwd.html"})
    public String userchangepwd(Integer userId,Model model){
        model.addAttribute("userId",userId);
        return "/system/user/changepwd";
    }
    /***
     * 用户密码修改
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "用户管理",type = 3)
    @SaCheckPermission("user:view:pwdmodify")
    @PostMapping({"/user/changepwd",})
    @ResponseBody
    public AjaxResult postuserchangepwd(@RequestBody User user){
        if (user.getUserId()==null){
            return error("密码修改失败");
        }
        if ("".equals(user.getPassword()) || user.getPassword()==null){
            return error("密码修改失败");
        }
        Boolean flag = userService.changepwdUser(user);
        if (flag){
            return success("密码修改成功");
        }
        return error("密码修改失败");
    }

}
