package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.system.monitor.CpuInfo;
import com.xiaozhi.xzadmin.tools.SystemUtil;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 23:01
 */
@Controller
@RequestMapping("/system/monitor/")
@SaCheckLogin
public class Sys_monitorController extends BaseController {


    /***
     * 系统检测页
     * @return
     */
    @AopUserOnline
    @GetMapping("main")
    @SaCheckPermission("monitor:view")
    public String main(Model model) {
        CpuInfo cpu = SystemUtil.getCpu();
        model.addAttribute("cpu", cpu);
        return "/system/monitor/main";
    }


}
