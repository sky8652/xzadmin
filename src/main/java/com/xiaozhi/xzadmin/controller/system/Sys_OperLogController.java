package com.xiaozhi.xzadmin.controller.system;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.pojo.system.OperLog;
import com.xiaozhi.xzadmin.service.sys.LogininforService;
import com.xiaozhi.xzadmin.service.sys.OpenlogService;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 23:01
 */
@Controller
@RequestMapping("/system/openlog")
@SaCheckLogin
public class Sys_OperLogController extends BaseController {


    @Autowired
    private OpenlogService openlogService;

    /***
     * 日志页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "操作日志",type = 0)
    @RequestMapping({"/",""})
    @SaCheckPermission("openlogquery:view")
    public String home(){
        return "/system/Openlog/openlog";
    }


    /***
     * 日志查询
     * @page 页码
     * @limit 页码长度
     * @operLog operLog实体类
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "操作日志",type = 0)
    @SaCheckPermission("openlogquery:view")
    @RequestMapping("/openlogquery")
    @ResponseBody
    public AjaxResult openlogquery(@RequestParam(value = "page", required = false,defaultValue = "1")Integer  page,
                                      @RequestParam(value = "limit", required = false,defaultValue = "5")Integer limit,
                                      OperLog operLog){
        Page<OperLog> OperLogPage = openlogService.getOpenlogPage(page,limit,operLog);
        return success(OperLogPage);
    }

}
