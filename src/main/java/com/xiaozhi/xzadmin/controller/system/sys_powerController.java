package com.xiaozhi.xzadmin.controller.system;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.Aop.AopUserOnline;
import com.xiaozhi.xzadmin.config.redis.RedisUtil;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.Permission;
import com.xiaozhi.xzadmin.service.sys.PermissionService;
import com.xiaozhi.xzadmin.tools.CookiesUtil;
import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/power")
@SaCheckLogin
public class sys_powerController extends BaseController {

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisUtil redisUtil;

    /***
     * 权限管理页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 0)
    @SaCheckPermission("power:view")
    @RequestMapping({"/",""})
    public String power(){
        return "/system/power/power";
    }

    /**
     * 获取个人主菜单信息
     * **/
    @AopUserOnline
    @RequestMapping("/menus")
    @ResponseBody
    public List<Permission> menus(HttpServletRequest request){
        Object rid = redisUtil.get(CookiesUtil.getCookieByName(request, "rid").getValue());
        if (rid==null){
            int uid = StpUtil.getLoginIdAsInt();
            return permissionService.getMenuList(uid);
        }
        return (List<Permission>) rid;
    }

    /**
     * 获取全部菜单信息
     * **/
    @AopUserOnline
    @RequestMapping("/menustree")
    @ResponseBody
    public AjaxResult menus1(){
        Object menus = redisUtil.get("menus");
        if (menus==null){
            return success(permissionService.getMenuList());
        }
        return success((List<Permission>) menus);
    }
    /**
     * 获取权限菜单
     * **/
    @AopUserOnline
    @SaCheckPermission("power:view:selects")
    @RequestMapping("/powerList")
    @ResponseBody
    public AjaxResult powerList(){
        return success(permissionService.powerList());
    }


    /***
     * 是否启用权限菜单
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 2)
    @SaCheckPermission("power:view:open")
    @RequestMapping("/enableupdate")
    @ResponseBody
    public AjaxResult offupdate(@RequestBody Permission permission) {
        Boolean flag = permissionService.isEnableupdateByid(permission);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }
    /***
     * 权限菜单添加页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 1)
    @SaCheckPermission("power:view:add")
    @GetMapping({"/add","/add.html"})
    public String permissionadd(){
        return "/system/power/add";
    }


    /***
     * 权限菜单添加
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 1)
    @SaCheckPermission("power:view:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult insertPermission(@RequestBody Permission permission) {
        if ("".equals(permission.getTitle()) || permission.getTitle()==null){
            return AjaxResult.error("权限菜单名不能为空");
        }
        if (permission.getParent_id()==null){
            permission.setParent_id(0);
        }
        if (permissionService.selectPermissionBytitle(permission)){
            return AjaxResult.error("权限菜单名已存在");
        }
        permission.setIcon("layui-icon "+permission.getIcon());
        permission.setId(permissionService.setPermissionid(permission));
        Boolean flag = permissionService.insetPermission(permission);
        if (flag){
            return success("添加成功");
        }
        return error("添加失败");
    }
    /***
     * 权限菜单修改页面
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 2)
    @SaCheckPermission("power:view:edit")
    @RequestMapping({"/edit","/edit.html"})
    public String roleedit(Integer id, Model model){
        Permission permission = permissionService.getPermissionByid(id);
        model.addAttribute("permission",permission);
        return "/system/power/edit";
    }
    /***
     * 权限菜单修改
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 2)
    @SaCheckPermission("power:view:edit")
    @RequestMapping("/save")
    @ResponseBody
    public AjaxResult savePermission(@RequestBody Permission permission) {
        Boolean flag = permissionService.updatePermissionByid(permission);
        if (flag){
            return success("修改成功");
        }
        return error("修改失败");
    }
    /***
     * 权限菜单删除
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 3)
    @SaCheckPermission("power:view:delete")
    @RequestMapping("/remove")
    @ResponseBody
    public AjaxResult removePermission(@RequestBody Map<String,String> map) {
        String ids = map.get("id");
        ArrayList<String> strings = StringUtil.cutString(ids);

        Boolean flag = permissionService.delPermissionByid(strings);
        if (flag){
            return success("删除成功");
        }
        return error("删除失败");
    }
    /***
     * 角色权限菜单查询
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 0)
    @SaCheckPermission("power:view:role:select")
    @GetMapping({"/getpowerchange","/getpowerchange.html"})
    public String getpowerchange(Integer rid,Model model){
        model.addAttribute("rid",rid);
        return "/system/power/getpowerchange";
    }

    /***
     * 根据rid查询Permission id
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 0)
    @SaCheckPermission("power:view:role:select")
    @PostMapping({"/getPermissionbyRid","/getPermissionbyRid.html"})
    @ResponseBody
    public AjaxResult getPermissionbyRid(@RequestBody Map<String,String> map){
        String rid = map.get("rid");
        List<Integer> integers = permissionService.selectByridList(Integer.valueOf(rid));
        if (integers.size()>0){
            return success(integers);
        }
        return error("参数错误");
    }

    /***
     * 添加角色菜单权限
     * @return
     */
    @AopUserOnline
    @AopLogger(title = "权限管理",type = 1)
    @SaCheckPermission("power:view:role:add")
    @RequestMapping("/changepowersByrid")
    @ResponseBody
    public AjaxResult changepowersByrid(@RequestBody Map<String,String> map) {
        String ids = map.get("id");
        ArrayList<String> strings = StringUtil.cutString(ids);
        String rid = map.get("rid");
        Boolean flag = permissionService.insertRoleToPromission(strings,rid);
        if (flag){
            return success("修改权限成功");
        }
        return error("修改权限失败");
    }
}
