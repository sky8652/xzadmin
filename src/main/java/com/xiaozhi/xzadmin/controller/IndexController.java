package com.xiaozhi.xzadmin.controller;

import cn.dev33.satoken.annotation.SaCheckBasic;
import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.basic.SaBasicUtil;
import cn.dev33.satoken.stp.StpUtil;
import com.xiaozhi.xzadmin.config.Aop.AopLogger;
import com.xiaozhi.xzadmin.config.redis.RedisUtil;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 21:41
 */
@Controller
@RequestMapping("/admin/index")
// 登录认证：只有登录之后才能进入该方法
@SaCheckLogin
public class IndexController extends BaseController {
    @Autowired
    private UserService userService;

    /**
     * 首页
     */
    @AopLogger(title = "首页",type = 0)
    @RequestMapping({"/",""})
    public String index(){
        return "/index/index";
    }

    /**
     * 控制后台
     */
    @AopLogger(title = "控制后台",type = 0)
    @RequestMapping({"/home","/home/"})
    public String home(){
        return "/index/home";
    }

    /**
     * 密码修改页面
     */
    @GetMapping({"/changpwd","/changpwd/"})
    public String changpwd(){
        return "/index/changpwd";
    }

    /**
     * 密码修改接口
     */
    @PostMapping("/changpwd")
    @ResponseBody
    public AjaxResult postuserchangepwd(@RequestBody Map<String,String> map){
        String oldpwd = map.get("oldpwd");
        String newpwd = map.get("newpwd");
        if ("".equals(oldpwd) || oldpwd==null){
            return error("旧密码为空");
        }
        if ("".equals(newpwd) || newpwd==null){
            return error("新密码未设置");
        }
        Boolean flag = userService.changepwdUserByid(oldpwd,newpwd);
        if (flag){
            StpUtil.logout();
            return success("密码修改成功");
        }
        return error("密码修改失败");
    }
}
