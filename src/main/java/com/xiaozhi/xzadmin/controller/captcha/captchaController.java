package com.xiaozhi.xzadmin.controller.captcha;

import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.wf.captcha.utils.CaptchaUtil;
import com.xiaozhi.xzadmin.pojo.CommonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class captchaController {

    @RequestMapping("/admin/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int width = 120;
        int height = 44;
        int len = 5;
        CommonUtil commonUtil = new CommonUtil();
        //验证码类型
        String codeType = commonUtil.getCodeType();
        //验证码字符串类型
        String charType = commonUtil.getCharType();
//        动态验证码
        if ("Gif".equals(codeType)){
            // 使用gif验证码
            GifCaptcha gifCaptcha = new GifCaptcha(width,height,len);
            switch(charType){
                case "TYPE_DEFAULT":
                    gifCaptcha.setCharType(Captcha.TYPE_DEFAULT);
                    break;
                case "TYPE_ONLY_NUMBER":
                    gifCaptcha.setCharType(Captcha.TYPE_ONLY_NUMBER);
                    break;
                case "TYPE_ONLY_UPPER":
                    gifCaptcha.setCharType(Captcha.TYPE_ONLY_UPPER);
                    break;
                case "TYPE_ONLY_LOWER":
                    gifCaptcha.setCharType(Captcha.TYPE_ONLY_LOWER);
                    break;
                case "TYPE_NUM_AND_UPPER":
                    gifCaptcha.setCharType(Captcha.TYPE_NUM_AND_UPPER);
                    break;
                default :
                    gifCaptcha.setCharType(Captcha.TYPE_ONLY_CHAR);
                    break;
            }
            CaptchaUtil.out(gifCaptcha, request, response);
        }else {
            //        静态验证码
                SpecCaptcha captcha = new SpecCaptcha(width,height,len);
                switch(charType){
                    case "TYPE_DEFAULT":
                        captcha.setCharType(Captcha.TYPE_DEFAULT);
                        break;
                    case "TYPE_ONLY_NUMBER":
                        captcha.setCharType(Captcha.TYPE_ONLY_NUMBER);
                        break;
                    case "TYPE_ONLY_UPPER":
                        captcha.setCharType(Captcha.TYPE_ONLY_UPPER);
                        break;
                    case "TYPE_ONLY_LOWER":
                        captcha.setCharType(Captcha.TYPE_ONLY_LOWER);
                        break;
                    case "TYPE_NUM_AND_UPPER":
                        captcha.setCharType(Captcha.TYPE_NUM_AND_UPPER);
                        break;
                    default :
                        captcha.setCharType(Captcha.TYPE_ONLY_CHAR);
                        break;
                }
                CaptchaUtil.out(captcha, request, response);

        }


    }


}
