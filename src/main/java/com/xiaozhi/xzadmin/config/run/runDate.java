package com.xiaozhi.xzadmin.config.run;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.xiaozhi.xzadmin.config.redis.RedisUtil;
import com.xiaozhi.xzadmin.mapper.sys.RoleMapper;
import com.xiaozhi.xzadmin.pojo.CommonUtil;
import com.xiaozhi.xzadmin.pojo.UserToRole;
import com.xiaozhi.xzadmin.service.sys.PermissionService;
import com.xiaozhi.xzadmin.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/4/5 11:23
 */
@Component
public class runDate implements ApplicationRunner {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    String salt = new CommonUtil().getSalt();
    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<UserToRole> userToRoles = roleService.selectAllnorepeatrid();
        for (UserToRole usertoRoles : userToRoles) {
//            设置个人菜单
            redisUtil.set(SaSecureUtil.md5BySalt(String.valueOf(usertoRoles.getRid()),salt),permissionService.getMenuList(usertoRoles.getUid()));
        }
        //设置选择菜单
        redisUtil.set("menus",permissionService.getMenuList());

    }

}
