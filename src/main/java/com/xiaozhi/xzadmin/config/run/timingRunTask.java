package com.xiaozhi.xzadmin.config.run;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.xiaozhi.xzadmin.config.redis.RedisUtil;
import com.xiaozhi.xzadmin.mapper.sys.RoleMapper;
import com.xiaozhi.xzadmin.pojo.CommonUtil;
import com.xiaozhi.xzadmin.pojo.UserToRole;
import com.xiaozhi.xzadmin.service.sys.PermissionService;
import com.xiaozhi.xzadmin.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/4/5 15:37
 */
@Component
public class timingRunTask {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    String salt = new CommonUtil().getSalt();
    /**
     *  每五分钟执行一次
     */
    @Scheduled(cron="0 0/5 * * * ? ")
    public void executeFileDownLoadTask() {
        List<UserToRole> userToRoles = roleService.selectAllnorepeatrid();
        for (UserToRole usertoRoles : userToRoles) {
//            设置个人菜单
            redisUtil.set(SaSecureUtil.md5BySalt(String.valueOf(usertoRoles.getRid()),salt),permissionService.getMenuList(usertoRoles.getUid()));
        }
        //设置选择菜单
        redisUtil.set("menus",permissionService.getMenuList());
    }
}
