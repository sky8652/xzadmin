package com.xiaozhi.xzadmin.config;

import cn.dev33.satoken.exception.*;
import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 21:16
 */
@Slf4j
@ControllerAdvice
public class MyExceptionHandler {

    // 全局异常拦截
    @ExceptionHandler
    public String handlerExceptions(Model model, RedirectAttributes attrs, Exception e, HttpServletRequest request) {

//        log.info("全局异常---------------");
        // 判断场景值，定制化异常信息
        String msg;
        if (e instanceof NotLoginException) {                   // 如果是未登录异常
            return "redirect:/admin/login";
        } else if (e instanceof NotRoleException) {             // 如果是角色异常
            NotRoleException ee = (NotRoleException) e;
            msg = "抱歉，你无权访问该页面";
        } else if (e instanceof NotPermissionException) {       // 如果是权限异常
            NotPermissionException ee = (NotPermissionException) e;
            msg = "抱歉，你无权访问该页面";
//        } else if (e instanceof DisableLoginException) {        // 如果是被封禁异常
//            DisableLoginException ee = (DisableLoginException) e;
//            msg = "账号被封禁：" + ee.getDisableTime() + "秒后解封";
        } else if (e instanceof NotSafeException) {             // 如果是二级认证
            NotSafeException ee = (NotSafeException) e;
            log.info("二级认证异常：" + ee.getMessage());
            StpUtil.openSafe(120);
            attrs.addFlashAttribute("msg","Please login to Confirm");
            attrs.addFlashAttribute("lastUrl",request.getRequestURL());
            return "redirect:/admin/login";
        } else if (e instanceof NotBasicAuthException) {        // 如果是Http Basic
            NotBasicAuthException ee = (NotBasicAuthException) e;
            msg = "抱歉，你无权访问该页面";
        } else {    // 普通异常, 输出：500 + 异常信息
            msg = e.getMessage();
        }
        model.addAttribute("exeMsg", msg);
        model.addAttribute("reqUrl", request.getRequestURL());
        return "/error/403";
    }

}
