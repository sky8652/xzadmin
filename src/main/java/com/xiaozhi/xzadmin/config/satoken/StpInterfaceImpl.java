package com.xiaozhi.xzadmin.config.satoken;

/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/3/19 13:34
 */


import cn.dev33.satoken.stp.StpInterface;
import com.xiaozhi.xzadmin.config.redis.RedisUtil;
import com.xiaozhi.xzadmin.mapper.sys.PermissionMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserToRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 自定义权限验证接口扩展
 */
@Component    // 保证此类被SpringBoot扫描，完成Sa-Token的自定义权限验证扩展
public class StpInterfaceImpl implements StpInterface {

    @Autowired
    private UserToRoleMapper userToRoleMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        Object o = redisUtil.get("getPermissionList" + loginId);
        List<String> lists = (List<String>) o;
        if (o==null){
            redisUtil.set("getPermissionList" + loginId, permissionMapper.selectByUidList(loginId),3600);
            return permissionMapper.selectByUidList(loginId);
        }
        return lists;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        Object o = redisUtil.get("getRoleList" + loginId);
        List<String> lists = (List<String>) o;
        if (o==null){
            redisUtil.set("getRoleList" + loginId, userToRoleMapper.selectByuidToRole(loginId),3600);
            return userToRoleMapper.selectByuidToRole(loginId);
        }
        return lists;
    }

}

