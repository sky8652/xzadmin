package com.xiaozhi.xzadmin.config.Aop;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AopUserOnline {
    String name() default "";
}
