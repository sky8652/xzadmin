package com.xiaozhi.xzadmin.config.Aop;


import cn.dev33.satoken.stp.StpUtil;
import com.xiaozhi.xzadmin.pojo.UserOnline;
import com.xiaozhi.xzadmin.service.sys.UserOnlineService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Component // springboot最基本的注解，表示把这个类交给spring来管理
public class UserOnlineAop {

    @Autowired
    private UserOnlineService userOnlineService;

    @Pointcut("@annotation(com.xiaozhi.xzadmin.config.Aop.AopUserOnline)")
    public void cutPoint(){};

    @Around("cutPoint()")
    public Object  doAround(ProceedingJoinPoint pjp) {
        Object proceed = null;
        try {
            // 执行目标对象的方法
            proceed = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        UserOnline userOnline = new UserOnline();
        //token
        userOnline.setSessionId(StpUtil.getTokenValue());
        userOnline.setLastAccessTime(new Date());
        userOnlineService.update(userOnline);
        return proceed;
    }

}
