package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
    /**
     * 递归查找个人菜单所有
     *
     * @return
     */
    public List<Permission> getMenuLists(@Param("parent_id")Integer parent_id,@Param("uid")Integer uid);

    /**
     * 递归查找所有菜单
     *
     * @return
     */
    public List<Permission> getMenuList();

    /**
     * 根据rid
     *
     * @return
     */
    public List<Integer> selectByridList(Integer rid);


    /**
     * 插入角色菜单权限
     *
     * @return
     */
    int insertRoleToPromission(@Param("promissionlist") ArrayList<String> promissionlist, @Param("rid") String rid);

    /**
     * 根据用户id查询获取权限列表
     *
     * @return
     */
    public List<String> selectByUidList(Object uid);

}

