package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.system.SystemFile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileMapper extends BaseMapper<SystemFile> {


}

