package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.UserOnline;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserOnlineMapper extends BaseMapper<UserOnline> {


}

