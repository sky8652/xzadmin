package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {


}

