package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.system.OperLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OperlogMapper extends BaseMapper<OperLog> {


}

