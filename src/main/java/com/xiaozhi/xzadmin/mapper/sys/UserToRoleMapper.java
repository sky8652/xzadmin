package com.xiaozhi.xzadmin.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserToRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface UserToRoleMapper extends BaseMapper<UserToRole> {


    /**
     * 插入角色和用户对应关系
     */
    int insertRoleToUser(@Param("useridlist")ArrayList<String> useridlist,@Param("rid") String rid);


    /**
     * 查询uid查询Role
     */
    List<String> selectByuidToRole(@Param("uid") Object uid);

    /**
     * 查询UserToRole不重复uid
     */
    List<UserToRole> selectAllnorepeatrid();


}

