package com.xiaozhi.xzadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 热更新、热加载
 * 1、Settings->Compiler->Build project automatically
 * 2、Ctrl+Shift+A ->搜索registry，找到Registry...，注意是后面有三个点的那个，然后找到compiler.automake.allow.when.app.running，勾选
 * 3、热部署快捷键Ctrl+F9,java代码修改后需要执行一下快捷键才能实际生效
 *
 */
@EnableScheduling
@SpringBootApplication
@ServletComponentScan
@MapperScan("com.xiaozhi.xzadmin.mapper")
public class XzadminApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext application = SpringApplication.run(XzadminApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String property = env.getProperty("server.servlet.context-path");
        String path = property == null ? "" :  property;
        System.out.println(
                "\n\t" +
                        "----------------------------------------------------------\n\t" +
                        "Application Sailrui-Boot is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                        "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                        "------------------------------------------------------------");
    }

}
