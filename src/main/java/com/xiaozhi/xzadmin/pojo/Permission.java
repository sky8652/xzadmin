package com.xiaozhi.xzadmin.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;
/**
 * 权限类
 *
 * @author zhangxiaozhi
 */
@TableName("sys_permission")
@Data
public class Permission {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;//菜单id

    @NotEmpty(message = "请输入标题！")
    @Column(length = 100)
    private String title; //菜单名

    @NotEmpty(message = "图标")
    @Column(length = 100)
    private String icon;//菜单图标

    @NotEmpty(message = "类别")
    @Column(length = 200)
    private String type; //0目录1菜单2按钮

    @NotEmpty(message = "排序")
    @Column(length = 200)
    private Integer sort; //排序

    @NotEmpty(message = "地址")
    @Column(length = 200)
    private String href; //菜单地址

    @NotEmpty(message = "是否开启")
    @Column(length = 200)
    private int enable; //菜单开启0关闭1开启

    @NotEmpty(message = "开启类别")
    @Column(length = 200)
    private String openType; //菜单开启类别

    @NotEmpty(message = "开启类别")
    @Column(length = 200)
    private String power_code; //权限标识

    @NotEmpty(message = "父id")
    @Column(length = 200)
    private Integer parent_id; //父id


    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date create_time;//创建时间

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date update_time;//更新时间

    // 子菜单
    @TableField(exist = false)
    private List<Permission> children;

}
