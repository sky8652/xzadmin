package com.xiaozhi.xzadmin.pojo;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 角色实体
 * @JsonIgnoreProperties 将这个注解写在类上之后，就会忽略类中不存在的字段
 */
@TableName("sys_roles")
@Data
public class Role implements Serializable {

    @TableId(value = "roleId",type = IdType.AUTO)
    private Integer roleId;//角色id

    @NotEmpty(message = "请输入用户名！")
    @Column(length = 100)
    private String roleName; //角色用户名

    @NotEmpty(message = "key值")
    @Column(length = 100)
    private String roleCode;//角色key值

    //是否被封禁
    private Integer is_open;//0未开启1开启

    @NotEmpty(message = "角色描述")
    @Column(length = 200)
    private String details; //角色描述

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date create_time;//创建时间

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date update_time;//更新时间
}
