package com.xiaozhi.xzadmin.pojo.system;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 文件管理类
 *
 * @author zhangxiaozhi
 */
/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022-04-06 16:20:14
 */
@TableName("sys_file")
@Data
public class SystemFile implements Serializable {

    /**
     * 标识
     */
    @TableId(value = "id",type = IdType.AUTO)
    private String id;
    /**
     * 文件名称
     */
    @TableField(value = "file_name")
    private String fileName;
    /**
     * 文件真实名称
     */
    @TableField(value = "file_realname")
    private String fileRealName;
    /**
     * 文件路径
     */
    @TableField(value = "file_path")
    private String filePath;
    /**
     * 文件类型
     */
    @TableField(value = "file_type")
    private String fileType;
    /**
     * 创建人
     */
    @TableField(value = "create_by")
    private String createBy;
    /**
     * 文件大小
     */
    @TableField(value = "file_size")
    private String fileSize;

    /** 访问时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;//创建时间




}

