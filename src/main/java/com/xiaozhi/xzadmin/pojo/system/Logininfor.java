package com.xiaozhi.xzadmin.pojo.system;


import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;


/**
 * 登录日志类
 *
 * @author zhangxiaozhi
 */
@TableName("sys_logininfor")
@Data
public class Logininfor
{

    /** 访问ID */
    @TableId(value = "info_id",type = IdType.AUTO)
    private Long infoId;

    /** 登录账号 */
    @NotEmpty(message = "登录账号")
    @TableField(value = "login_name")
    private String loginName;

    /** 登录IP地址 */
    @NotEmpty(message = "登录IP地址")
    private String ipaddr;

    /** 登录地点 */
    @NotEmpty(message = "登录地点")
    @TableField(value = "login_location")
    private String loginLocation;

    /** 浏览器类型 */
    @NotEmpty(message = "浏览器类型")
    private String browser;

    /** 操作系统 */
    @NotEmpty(message = "操作系统")
    private String os;

    /** 登录状态（0成功 1失败） */
    @NotEmpty(message = "登录状态")
    private String status;

    /** 提示消息 */
    @NotEmpty(message = "提示消息")
    private String msg;

    /** 访问时间 */
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date create_time;//创建时间

    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date end_time;//结束时间


}

