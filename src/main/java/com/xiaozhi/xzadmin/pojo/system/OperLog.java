package com.xiaozhi.xzadmin.pojo.system;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 操作日志类
 *
 * @author zhangxiaozhi
 */
/**
 * @Author: zhangxiaozhi
 * @Description: 1.0
 * @Date: create in 2022/4/4 23:54
 */
@TableName("sys_oper_log")
@Data
public class OperLog {

    /** 日志主键 */
    @TableId(value = "oper_id",type = IdType.AUTO)
    private Long operId;

    /** 模块标题 */
    private String title;

    /** 业务类型（0其它 1新增 2修改 3删除 4授权 5强退） */
    @TableField(value = "business_type")
    private Integer businessType;

    /** 方法名称 */
    private String method;

    /** 请求方式 */
    @TableField(value = "request_method")
    private String requestMethod;


    /** 操作人员 */
    @TableField(value = "oper_name")
    private String operName;


    /** 请求URL */
    @TableField(value = "oper_url")
    private String operUrl;

    /** 主机地址 */
    @TableField(value = "oper_ip")
    private String operIp;

    /** 操作地点 */
    @TableField(value = "oper_location")
    private String operLocation;

    /** 请求参数 */
    @TableField(value = "oper_param")
    private String operParam;

    /** 返回参数 */
    @TableField(value = "json_result")
    private String jsonResult;

    /** 操作状态（0正常 1异常） */
    private Integer status;

    /** 错误消息 */
    @TableField(value = "error_msg")
    private String errorMsg;

    /** 访问时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;//创建时间

    @TableField(exist = false)
    private Date end_time;//结束时间
}
