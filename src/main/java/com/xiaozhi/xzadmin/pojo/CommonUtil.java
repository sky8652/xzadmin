package com.xiaozhi.xzadmin.pojo;

import lombok.Data;

/**
 * 常用类
 *
 * @author zhangxiaozhi
 */
@Data
public class CommonUtil {

    /**
     * 文件上传路径（写在static）
    */
    private String filepath = System.getProperty("user.dir")+"/src/main/resources/static/static/file/";
    /**
     * redisName salt
     */
    private String salt="xiaozhi";
    /**
     * 验证码是否开启
     */
    private Boolean codeisOpen=false;
    /**
     * 验证码类型
     * Spec 静态png
     * Gif  gif类型
     * 默认为gif
     */
    private String codeType = "Gif";
    /**
     * 验证码字符类型
     * TYPE_DEFAULT 数字和字母混合
     * TYPE_ONLY_NUMBER  纯数字
     * TYPE_ONLY_CHAR  纯字母
     * TYPE_ONLY_UPPER  纯大写字母
     * TYPE_ONLY_LOWER  纯小写字母
     * TYPE_NUM_AND_UPPER  数字和大写字母
     * 默认为纯字母
     */
    private String charType="TYPE_DEFAULT";

}
