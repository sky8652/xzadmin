package com.xiaozhi.xzadmin.pojo;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户实体
 * @JsonIgnoreProperties 将这个注解写在类上之后，就会忽略类中不存在的字段
 */
@TableName("sys_user")
@Data
public class User{

    @TableId(value = "userId",type = IdType.AUTO)
    private Integer userId;//用户id

    @NotEmpty(message = "请输入用户名！")
    @Column(length = 100)
    private String username; //用户名

    @NotEmpty(message = "请输入密码！")
    @Column(length = 100)
    private String password;//密码

    @NotEmpty(message = "昵称不能为空！")
    @Column(length = 200)
    private String nick_name; //昵称

    @Column(length = 50)
    private Integer sex;//性别0男1女2未知

    @Column(length = 11)
    private String phone;//电话号码

    //是否被封禁
    private Integer is_off = 0;//0未封禁1封禁

    @Email(message = "邮箱地址格式有误！")
    @NotEmpty(message = "请输入邮箱地址！")
    @Column(length = 100)
    private String email;//邮箱地址

    @Column(length = 100)
    private String avatar; //用户头像

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date create_time;//创建时间

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date update_time;//更新时间

    /**myabtis @TableField表示不是数据库字段*/
    @TableField(exist = false)
    private String verCode;
    /**myabtis @TableField表示不是数据库字段*/
    @TableField(exist = false)
    private Boolean remember;

}
