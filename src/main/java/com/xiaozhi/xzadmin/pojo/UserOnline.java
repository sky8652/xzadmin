package com.xiaozhi.xzadmin.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 在线用户记录对象 sys_user_online
 *
 * @author xiaozhi
 * @date 2022-05-09
 */
@TableName("sys_user_online")
@Data
public class UserOnline {


    /** 用户会话id */
    @TableId(value = "usernolineId",type = IdType.AUTO)
    private String usernolineId;

    /** 用户会话sseionid */
    private String sessionId;

    /** 登录账号 */
    @TableField(value = "login_name")
    private String loginName;


    /** 登录IP地址 */
    @TableField(value = "ipaddr")
    private String ipaddr;

    /** 登录地点 */
    @TableField(value = "login_location")
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** 在线状态on_line在线off_line离线 */
    private String status;

    /** session创建时间 */
    @TableField(fill = FieldFill.INSERT,value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTimestamp;

    /** session最后访问时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE,value = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastAccessTime;

}

