package com.xiaozhi.xzadmin.pojo;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户角色对应关系实体
 * @JsonIgnoreProperties 将这个注解写在类上之后，就会忽略类中不存在的字段
 */
@TableName("sys_urs")
@Data
public class UserToRole implements Serializable {

    @TableField(value = "uid")
    private Integer uid;//用户id


    @TableField(value = "rid")
    private Integer rid;//角色id

}
