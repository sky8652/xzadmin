package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;

import java.util.ArrayList;
import java.util.List;

/**
 * 登录日志
 */
public interface LogininforService {
    /**
     * 创建登录日志
     * @return
     */
    public Boolean insetlogininfor(Logininfor logininfor);

    /**
     * 根据分页查询
     * @current page          当前页
     * @size size          长度
     * @logininfor logininfor实体类
     * @return
     */
    public Page<Logininfor> getLogininforPage(Integer page,Integer size,Logininfor logininfor);


}
