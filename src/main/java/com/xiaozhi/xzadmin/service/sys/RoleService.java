package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserToRole;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色Service实现类
 */
public interface RoleService {


    /**
     * 根据分页查询
     * @current current          当前页
     * @size size          长度
     * @roleName roleName 角色名
     * @return
     */
    public Page<Role> getRolePage(Integer current, Integer size, String roleName);


    /**
     * 根据id和值封禁账号
     * @return
     */
    public Boolean isOpenupdateByid(Role role);

    /**
     * 判断角色名是否重复
     * @return
     */
    public boolean selectRoleByname(Role role);
    /**
     * 创建角色
     * @return
     */
    public Boolean insetRole(Role role);
    /**
     * 获取修改信息
     * @id id 角色id
     * @return
     */
    public Role getRoleByid(Integer id);

    /**
     * 修改角色信息
     * @id id 角色id
     * @return
     */
    public Boolean updateRoleByid(Role role);

    /**
     * 删除角色信息
     * @id id 角色id
     * @return
     */
    public Boolean delRoleByid(ArrayList<String> list);

    /**
     * 获取角色授权用户总数
     * @id rid 角色rid
     * @return
     */
    public Boolean queryUserByRoleidCount(Integer rid);
    /**
     * 获取角色授权对应用户
     * @id rid 角色rid
     * @return
     */
    public Page<User> getUserByRoleid(Integer page,Integer size,Integer rid,String username,Integer phone);
    /**
     * 取消授权信息
     * @id rid 角色rid
     * @id uid 角色uid
     * @return
     */
    public Boolean delRoleToUserByid(ArrayList<String> useridlist, String rid);
    /**
     * 获取角色未授权对应用户
     * @id rid 角色rid
     * @return
     */
    public Page<User> getNotUserByRoleid(Integer page,Integer size,String username,Integer phone);
    /**
     * 添加授权信息
     * @id rid 角色rid
     * @id uid 角色uid
     * @return
     */
    public Boolean addRoleToUserByid(ArrayList<String> useridlist, String rid);

    /**
     * 获取rid
     * @id uid 角色uid
     * @return
     */
    public Integer getridByrid(Integer uid);

    /**
     * 获取不重复rid下的不同uid
     * @return
     */
    public List<UserToRole> selectAllnorepeatrid();

}
