package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.Permission;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
/**
 * 权限Service实现类
 */
public interface PermissionService {
    /**
     * 获取全部菜单
     * @return
     */
    public List<Permission> getMenuList();
        /**
     * 获取用户个人菜单
     * @return
     */
     public List<Permission> getMenuList(Integer uid);
    /**
     * 获取权限菜单
     * @return
     */
    public List<Permission> powerList();


    /**
     * 根据id和值封禁账号
     * @return
     */
    public Boolean isEnableupdateByid(Permission permission);

    /**
     * 判断权限名是否存在
     * @return
     */
    public boolean selectPermissionBytitle(Permission permission);
    /**
     * 权限id设置权限菜单
     * @return
     */
    public Integer setPermissionid(Permission permission);
    /**
     *
     * 创建权限菜单
     * @return
     */
    public Boolean insetPermission(Permission permission);
    /**
     * 获取修改权限菜单信息
     * @id id 权限菜单id
     * @return
     */
    public Permission getPermissionByid(Integer id);

    /**
     * 修改权限菜单信息
     * @return
     */
    public Boolean updatePermissionByid(Permission permission);

    /**
     * 删除权限菜单信息
     *
     * @return
     */
    public Boolean delPermissionByid(ArrayList<String> list);

    /**
     * 根据rid查询Permission id
     *
     * @return
     */
    public List<Integer> selectByridList(Integer rid);

    /**
     * 插入角色菜单权限
     *
     * @return
     */
    public Boolean insertRoleToPromission(@Param("promissionlist") ArrayList<String> promissionlist, @Param("rid") String rid);

}
