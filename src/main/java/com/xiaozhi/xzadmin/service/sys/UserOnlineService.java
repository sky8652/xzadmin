package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserOnline;

import java.util.ArrayList;
import java.util.List;
/**
 * 用户Service实现类
 */
public interface UserOnlineService {

        /**
     * 保存用户在线数据
     * @return
     */
    public void save(UserOnline userOnline);
    /**
     * 修改用户在线数据
     * @return
     */
    public void update(UserOnline userOnline);

    /**
     * 根据分页查询
     * @current current          当前页
     * @size size          长度
     * @username username 登录用户名
     * @return
     */
    public Page<UserOnline> getUserOnlinePage(Integer current,Integer size,String loginmame,String ipaddr);

}
