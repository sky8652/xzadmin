package com.xiaozhi.xzadmin.service.sys.impl;


import cn.dev33.satoken.secure.SaSecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.RoleMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserToRoleMapper;
import com.xiaozhi.xzadmin.pojo.Role;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserToRole;
import com.xiaozhi.xzadmin.service.sys.RoleService;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.numberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * 角色Service实现类
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserToRoleMapper userToRoleMapper;



    @Override
    public Page<Role> getRolePage(Integer page,Integer size,String roleName) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<Role> pages = new Page<>(page, size);
        QueryWrapper<Role> wrapper = new QueryWrapper<>();
        if (roleName!=null && !"".equals(roleName)){
            wrapper.like("roleName",roleName);
        }
        // 忽略下面字段
        Page<Role> rolePage = roleMapper.selectPage(pages, wrapper);
        return rolePage;
    }

    @Override
    public Boolean isOpenupdateByid(Role role) {
        boolean flag = false;
        int is_open = role.getIs_open();
        Integer roleId = role.getRoleId();
        if ("null".equals(String.valueOf(roleId))){
            return false;
        }
        if (is_open ==1 || is_open ==0 || !"null".equals(String.valueOf(is_open))){
            UpdateWrapper<Role> wrapper = new UpdateWrapper<>();
            wrapper.eq("roleId",roleId);
            int inserts = roleMapper.update(role,wrapper);
            if (inserts>0){
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean selectRoleByname(Role role) {
        boolean flag = false;
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put("roleName",role.getRoleName());
        List<Role> roles = roleMapper.selectByMap(stringHashMap);
        if (roles.size()>0){
            flag =true;
        }
        return flag;
    }



    @Override
    public Boolean insetRole(Role role) {
        boolean flag = false;
        int insert = roleMapper.insert(role);
        if (insert>0){
            flag =true;
        }
        return flag;
    }

    @Override
    public Role getRoleByid(Integer roleId) {
        return roleMapper.selectById(roleId);
    }
    @Override
    public Boolean updateRoleByid(Role user) {
        Boolean flag = false;
        int updateid = roleMapper.updateById(user);
        if (updateid>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean delRoleByid(ArrayList<String> list) {
        Boolean flag = false;
        int i = roleMapper.deleteBatchIds(list);
        if (i>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean queryUserByRoleidCount(Integer rid) {
        Boolean flag = false;
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("rid",rid);
        Integer integer = userToRoleMapper.selectCount(wrapper);
        if (integer !=0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Page<User> getUserByRoleid(Integer page,Integer size,Integer rid,String username,Integer phone) {
//        查询rid列表
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("rid",rid);
        List<UserToRole> userToRoles = userToRoleMapper.selectByMap(hashMap);
        ArrayList<Integer> stringid = StringUtil.cutList(userToRoles);

        Page<User> pages = new Page<>(page, size);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.in("userId",stringid);
        if (username!=null && !"".equals(username)){
            wrapper.like("username",username);
        }
        if (phone!=null){
            wrapper.like("phone",phone);
        }
        Page<User> userPage = userMapper.selectPage(pages, wrapper);

        return userPage;
    }

    @Override
    public Boolean delRoleToUserByid(ArrayList<String> useridlist, String rid) {
        Boolean flag = false;
        if (useridlist.size()==0){
            return flag;
        }
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.in("uid",useridlist);
        wrapper.eq("rid",rid);
        int i = userToRoleMapper.delete(wrapper);
        if (i>0){
            flag = true;
        }
        return flag;
    }
    @Override
    public Page<User> getNotUserByRoleid(Integer page,Integer size,String username,Integer phone) {

        Page<User> pages = new Page<>(page, size);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if (username!=null && !"".equals(username)){
            wrapper.like("username",username);
        }
        if (phone!=null){
            wrapper.like("phone",phone);
        }
        wrapper.notInSql("userId", "select uid from sys_urs");

        return userMapper.selectPage(pages, wrapper);
    }

    @Override
    public Boolean addRoleToUserByid(ArrayList<String> useridlist, String rid) {
        Boolean flag = false;
        if (useridlist.size()==0){
            return flag;
        }
        int i = userToRoleMapper.insertRoleToUser(useridlist,rid);
        if (i>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Integer getridByrid(Integer uid) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("uid",uid);
        return userToRoleMapper.selectByMap(hashMap).get(0).getRid();
    }

    @Override
    public List<UserToRole> selectAllnorepeatrid() {
        return userToRoleMapper.selectAllnorepeatrid();
    }
}














