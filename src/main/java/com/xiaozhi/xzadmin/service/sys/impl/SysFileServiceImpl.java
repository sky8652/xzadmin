package com.xiaozhi.xzadmin.service.sys.impl;


import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.FileMapper;
import com.xiaozhi.xzadmin.mapper.sys.LogininforMapper;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.pojo.system.SystemFile;
import com.xiaozhi.xzadmin.service.sys.SysFileService;
import com.xiaozhi.xzadmin.tools.numberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 文件管理Service实现类
 */
@Service
public class SysFileServiceImpl implements SysFileService {

    @Autowired
    private FileMapper fileMapper;

    @Override
    public Boolean insetsystemFile(SystemFile systemFile) {
        boolean flag = false;
        int insert = fileMapper.insert(systemFile);
        if (insert>0){
            flag =true;
        }
        return flag;
    }

    @Override
    public Page<SystemFile> getSystemFilePage(Integer page,Integer size,SystemFile systemFile) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<SystemFile> pages = new Page<>(page, size);
        QueryWrapper<SystemFile> wrapper = new QueryWrapper<>();
        if (systemFile.getFileName()!=null && !"".equals(systemFile.getFileName())){
            wrapper.like("file_name",systemFile.getFileName());
        }
        if (systemFile.getFileRealName()!=null && !"".equals(systemFile.getFileRealName())){
            wrapper.like("file_realname",systemFile.getFileRealName());
        }
        wrapper.eq("create_by", StpUtil.getLoginIdAsString());
        wrapper.orderByDesc("create_time");
        Page<SystemFile> filePage = fileMapper.selectPage(pages, wrapper);
        return filePage;
    }

    @Override
    public List<SystemFile> getSystemFileByid(ArrayList<String> list) {
        List<SystemFile> systemFiles = fileMapper.selectBatchIds(list);
        return systemFiles;
    }

    @Override
    public Boolean delSystemFileByid(ArrayList<String> list) {
        Boolean flag = false;
        int i = fileMapper.deleteBatchIds(list);
        if (i>0){
            flag = true;
        }
        return flag;
    }
}














