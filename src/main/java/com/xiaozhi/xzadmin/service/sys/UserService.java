package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户Service实现类
 */
public interface UserService {

    /**
     * 根据用户名和密码查用户
     * @return
     */
    public List<User> getUsernametopassword(User user);

    /**
     * 根据分页查询
     * @current current          当前页
     * @size size          长度
     * @username username 用户名
     * @email email 邮箱
     * @sex sex 性别
     * @return
     */
    public Page<User> getUserPage(Integer current,Integer size,String username,String email,Integer sex);

    /**
     * 根据id和值封禁账号
     * @return
     */
    public Boolean isOffupdateByid(User user);

    /**
     * 判断用户名是否重复
     * @return
     */
    public boolean selectUserByusername(User user);
    /**
     * 创建用户
     * @return
     */
    public Boolean insetUser(User user);
    /**
     * 获取修改信息
     * @id id 用户id
     * @return
     */
    public User getUserByid(Integer id);

    /**
     * 修改用户信息
     * @id id 用户id
     * @return
     */
    public Boolean updateUserByid(User user);

    /**
     * 删除用户信息
     * @id id 用户id
     * @return
     */
    public Boolean delUserByid(ArrayList<String> list);

    /**
     * 修改用户密码
     * @id id 用户id
     * @password password 用户密码
     * @return
     */
    public Boolean changepwdUser(User user);
    /**
     * 修改用户密码
     * @id id 用户id
     * @password password 用户旧密码
     * @return
     */
    public Boolean changepwdUserByid(String oldpwd,String newpwd);
}
