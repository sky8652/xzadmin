package com.xiaozhi.xzadmin.service.sys.impl;


import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.LogininforMapper;
import com.xiaozhi.xzadmin.mapper.sys.OperlogMapper;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.pojo.system.OperLog;
import com.xiaozhi.xzadmin.service.sys.LogininforService;
import com.xiaozhi.xzadmin.service.sys.OpenlogService;
import com.xiaozhi.xzadmin.tools.numberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * 操作日志Service实现类
 */
@Service
public class OpenlogServiceImpl implements OpenlogService {
    @Autowired
    private OperlogMapper operlogMapper;

    @Override
    public Boolean insetopenlog(OperLog operLog) {
        boolean flag = false;
        int insert = operlogMapper.insert(operLog);
        if (insert>0){
            flag =true;
        }
        return flag;
    }

    @Override
    public Page<OperLog> getOpenlogPage(Integer page,Integer size,OperLog operLog) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<OperLog> pages = new Page<>(page, size);
        QueryWrapper<OperLog> wrapper = new QueryWrapper<>();
        if (operLog.getTitle()!=null && !"".equals(operLog.getTitle())){
            wrapper.like("title",operLog.getTitle());
        }
        if (operLog.getOperName()!=null && !"".equals(operLog.getOperName())){
            wrapper.like("oper_name",operLog.getOperName());
        }
        if (operLog.getBusinessType()!=null){
            wrapper.eq("business_type",operLog.getBusinessType());
        }
        if (operLog.getStatus()!=null){
            wrapper.eq("status",operLog.getStatus());
        }

        if (operLog.getCreate_time()!=null && operLog.getEnd_time()!=null){
            Date beginOfDay = DateUtil.parse(DateUtil.formatDate(operLog.getCreate_time()));
            Date getEnd_time = DateUtil.parse(DateUtil.formatDate(operLog.getEnd_time()));
            if (DateUtil.between(beginOfDay, getEnd_time, DateUnit.DAY,false)>=0){
                wrapper.ge("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(beginOfDay));
                wrapper.le("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(getEnd_time));
            }else {
                wrapper.ge("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(getEnd_time));
                wrapper.le("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(beginOfDay));
            }

        }
        wrapper.orderByDesc("create_time");
        Page<OperLog> OperLogPage = operlogMapper.selectPage(pages, wrapper);
        return OperLogPage;
    }
}














