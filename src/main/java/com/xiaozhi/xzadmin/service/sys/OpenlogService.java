package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.pojo.system.OperLog;

/**
 * 系统日志
 */
public interface OpenlogService {
    /**
     * 创建系统日志
     * @return
     */
    public Boolean insetopenlog(OperLog operLog);

    /**
     * 根据分页查询
     * @current current          当前页
     * @size size          长度
     * @logininfor logininfor 实体类
     * @return
     */
    public Page<OperLog> getOpenlogPage(Integer page,Integer size,OperLog operLog);

}
