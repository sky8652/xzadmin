package com.xiaozhi.xzadmin.service.sys.impl;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.UserOnlineMapper;
import com.xiaozhi.xzadmin.pojo.UserOnline;
import com.xiaozhi.xzadmin.service.sys.UserOnlineService;
import com.xiaozhi.xzadmin.tools.numberUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * 用户Service实现类
 */
@Service
@Slf4j
public class UserOnlineServiceImpl implements UserOnlineService {

    @Autowired
    private UserOnlineMapper userOnlineMapper;

    @Override
    public void save(UserOnline userOnline) {
        userOnlineMapper.insert(userOnline);

    }

    @Override
    public void update(UserOnline userOnline) {
            QueryWrapper<UserOnline> wrapper = new QueryWrapper<>();
            wrapper.eq("sessionId",userOnline.getSessionId());
            userOnlineMapper.update(userOnline,wrapper);
    }

    @Override
    public Page<UserOnline> getUserOnlinePage(Integer page,Integer size,String loginname,String ipaddr) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<UserOnline> pages = new Page<>(page, size);
        QueryWrapper<UserOnline> wrapper = new QueryWrapper<>();
        if (loginname!=null && !"".equals(loginname)){
            wrapper.like("login_name",loginname);
        }
        if (ipaddr!=null && !"".equals(ipaddr)){
            wrapper.like("ipaddr",ipaddr);
        }
        wrapper.orderByDesc("update_time");
        return userOnlineMapper.selectPage(pages, wrapper);
    }
}














