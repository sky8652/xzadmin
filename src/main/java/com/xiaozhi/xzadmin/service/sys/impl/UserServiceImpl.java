package com.xiaozhi.xzadmin.service.sys.impl;



import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.UserMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserToRoleMapper;
import com.xiaozhi.xzadmin.pojo.AjaxResult;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.UserToRole;
import com.xiaozhi.xzadmin.service.sys.UserService;

import com.xiaozhi.xzadmin.tools.StringUtil;
import com.xiaozhi.xzadmin.tools.numberUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


/**
 * 用户Service实现类
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserToRoleMapper userToRoleMapper;

    @Override
    public List<User> getUsernametopassword(User user) {
        List<User> users = null;
        String user_name = user.getUsername();
        String password = user.getPassword();
        if (user_name.equals("") ||password.equals("")){
            return users;
        }
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put("username",user_name);
        stringHashMap.put("password", SaSecureUtil.md5(password));
        users = userMapper.selectByMap(stringHashMap);
        return users;
    }

    @Override
    public Page<User> getUserPage(Integer page,Integer size,String username,String email,Integer sex) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<User> pages = new Page<>(page, size);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if (username!=null && !"".equals(username)){
            wrapper.like("username",username);
        }
        if (sex!=null){
            wrapper.eq("sex",sex);
        }
        if (email!=null && !"".equals(email)){
            wrapper.like("email",email);
        }
        // 忽略下面字段
        wrapper.select(User.class,i -> !i.getColumn().equals("password") && !i.getColumn().equals("update_time"));
        Page<User> userPage = userMapper.selectPage(pages, wrapper);
        return userPage;
    }

    @Override
    public Boolean isOffupdateByid(User user) {
        boolean flag = false;
        int is_off = user.getIs_off();
        Integer userId = user.getUserId();
        if (userId==1){
            return false;
        }
        if ("null".equals(String.valueOf(userId))){
            return false;
        }
        if (is_off ==1 || is_off ==0 || !"null".equals(String.valueOf(is_off))){
            UpdateWrapper<User> wrapper = new UpdateWrapper<>();
            wrapper.eq("userId",userId);
            int inserts = userMapper.update(user,wrapper);
            if (inserts>0){
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean selectUserByusername(User user) {
        boolean flag = false;
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put("username",user.getUsername());
        List<User> users = userMapper.selectByMap(stringHashMap);
        if (users.size()>0){
            flag =true;
        }
        return flag;
    }



    @Override
    public Boolean insetUser(User user) {
        boolean flag = false;
        user.setPassword(SaSecureUtil.md5(user.getPassword()));
        int insert = userMapper.insert(user);
        if (insert>0){
            UserToRole userToRole = new UserToRole();
            userToRole.setUid(user.getUserId());
//            默认设置用户为普通用户权限
            userToRole.setRid(3);
            userToRoleMapper.insert(userToRole);
            flag =true;
        }
        return flag;
    }

    @Override
    public User getUserByid(Integer id) {
        return userMapper.selectById(id);
    }
    @Override
    public Boolean updateUserByid(User user) {
        Boolean flag = false;
        if (user.getUserId()==1){
            return flag;
        }
        user.setUsername(null);
        user.setPassword(null);
        int updateid = userMapper.updateById(user);
        if (updateid>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean delUserByid(ArrayList<String> list) {
        Boolean flag = false;
        for (String s : list) {
            if ("1".equals(s)){
                return false;
            }
        }
        int i = userMapper.deleteBatchIds(list);
        if (i>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean changepwdUser(User user) {
        Boolean flag = false;
        user.setPassword(SaSecureUtil.md5(user.getPassword()));
        int i = userMapper.updateById(user);
        if (i>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean changepwdUserByid(String oldpwd, String newpwd) {
        Boolean flag = false;
        User user = new User();
        user.setPassword(SaSecureUtil.md5(newpwd));
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("userId",StpUtil.getLoginIdAsInt());
        wrapper.eq("password",SaSecureUtil.md5(oldpwd));
        int i = userMapper.update(user, wrapper);
        if (i>0){
            flag = true;
        }
        return flag;
    }
}














