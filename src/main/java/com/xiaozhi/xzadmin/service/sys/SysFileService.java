package com.xiaozhi.xzadmin.service.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.pojo.system.SystemFile;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件管理Service实现类
 */
public interface SysFileService {
    /**
     * 创建文件列表日志
     * @return
     */
    public Boolean insetsystemFile(SystemFile systemFile);

    /**
     * 根据分页查询
     * @current page          当前页
     * @size size          长度
     * @systemFile systemFile实体类
     * @return
     */
    public Page<SystemFile> getSystemFilePage(Integer page,Integer size,SystemFile systemFile);

    /**
     * 获取文件信息
     * @id id 用户id
     * @return
     */
    public List<SystemFile> getSystemFileByid(ArrayList<String> list);
    /**
     * 删除文件信息
     * @id id 用户id
     * @return
     */
    public Boolean delSystemFileByid(ArrayList<String> list);





}
