package com.xiaozhi.xzadmin.service.sys.impl;


import cn.dev33.satoken.secure.SaSecureUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozhi.xzadmin.mapper.sys.LogininforMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserMapper;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.pojo.system.Logininfor;
import com.xiaozhi.xzadmin.service.sys.LogininforService;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.numberUtil;
import com.xiaozhi.xzadmin.tools.web.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * 登录日志Service实现类
 */
@Service
public class LogininforServiceImpl implements LogininforService {
    @Autowired
    private LogininforMapper logininforMapper;

    @Override
    public Boolean insetlogininfor(Logininfor logininfor) {
        boolean flag = false;
        int insert = logininforMapper.insert(logininfor);
        if (insert>0){
            flag =true;
        }
        return flag;
    }

    @Override
    public Page<Logininfor> getLogininforPage(Integer page,Integer size,Logininfor logininfor) {
        if (page<0){
            page = 1;
        }
        if (size<0 || !new numberUtil().getNum(size)){
            size = 5;
        }
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单了！
        Page<Logininfor> pages = new Page<>(page, size);
        QueryWrapper<Logininfor> wrapper = new QueryWrapper<>();
        if (logininfor.getLoginName()!=null && !"".equals(logininfor.getLoginName())){
            wrapper.like("login_name",logininfor.getLoginName());
        }
        if (logininfor.getStatus()!=null &&!"".equals(logininfor.getStatus())){
            wrapper.eq("status",logininfor.getStatus());
        }
        if (logininfor.getLoginLocation()!=null && !"".equals(logininfor.getLoginLocation())){
            wrapper.like("login_location",logininfor.getLoginLocation());
        }

        if (logininfor.getCreate_time()!=null && logininfor.getEnd_time()!=null){
            Date beginOfDay = DateUtil.parse(DateUtil.formatDate(logininfor.getCreate_time()));
            Date getEnd_time = DateUtil.parse(DateUtil.formatDate(logininfor.getEnd_time()));
            if (DateUtil.between(beginOfDay, getEnd_time, DateUnit.DAY,false)>=0){
                wrapper.ge("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(beginOfDay));
                wrapper.le("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(getEnd_time));
            }else {
                wrapper.ge("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(getEnd_time));
                wrapper.le("date_format(create_time,'%Y-%m-%d')", DateUtil.formatDate(beginOfDay));
            }

        }
        wrapper.orderByDesc("create_time");
        Page<Logininfor> userPage = logininforMapper.selectPage(pages, wrapper);
        return userPage;
    }

}














