package com.xiaozhi.xzadmin.service.sys.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiaozhi.xzadmin.mapper.sys.PermissionMapper;
import com.xiaozhi.xzadmin.pojo.Permission;
import com.xiaozhi.xzadmin.service.sys.PermissionService;
import com.xiaozhi.xzadmin.tools.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * 权限Service实现类
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;


    @Override
    public List<Permission> getMenuList() {
        return permissionMapper.getMenuList();
    }

    @Override
    public List<Permission> getMenuList(Integer uid) {
        return getAllCommentByBlogId(0,uid);
    }

    @Override
    public List<Permission> powerList() {
        QueryWrapper<Permission> wrapper = new QueryWrapper();
        wrapper.orderByAsc("sort");
        return permissionMapper.selectList(wrapper);
    }


    @Override
    public Boolean isEnableupdateByid(Permission permission) {
        boolean flag = false;
        int enable = permission.getEnable();
        Integer id = permission.getId();
        if ("null".equals(String.valueOf(id))){
            return false;
        }
        if (enable == 1 || enable == 0 || !"null".equals(String.valueOf(enable))){
            UpdateWrapper<Permission> wrapper = new UpdateWrapper<>();
            wrapper.eq("id",id);
            int inserts = permissionMapper.update(permission,wrapper);
            if (inserts>0){
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean selectPermissionBytitle(Permission permission) {
        boolean flag = false;
        HashMap<String, Object> stringHashMap = new HashMap<>();
        stringHashMap.put("title",permission.getTitle());
        stringHashMap.put("parent_id",permission.getParent_id());
        List<Permission> permissions = permissionMapper.selectByMap(stringHashMap);
        if (permissions.size()>0){
            flag =true;
        }
        return flag;
    }


    @Override
    public Integer setPermissionid(Permission permission) {
        if (permission.getParent_id()==null){
            permission.setId(0);
        }
        QueryWrapper<Permission> wrapper = new QueryWrapper();
        wrapper.eq("parent_id",permission.getParent_id());
        wrapper.orderByAsc("id");
        List<Permission> permissionList = permissionMapper.selectList(wrapper);
        if (permissionList.size()==0){
            return Integer.valueOf(permission.getParent_id()+"01");
        }
        return permissionList.get(permissionList.size()-1).getId() + 1;
    }

    @Override
    public Boolean insetPermission(Permission permission) {
        boolean flag = false;
        int insert = permissionMapper.insert(permission);
        if (insert>0){
            flag =true;
        }
        return flag;
    }

    @Override
    public Permission getPermissionByid(Integer roleId) {
        return permissionMapper.selectById(roleId);
    }
    @Override
    public Boolean updatePermissionByid(Permission permission) {
        Boolean flag = false;
        String icon = "layui-icon";
        if (StringUtil.findCount(permission.getIcon(),icon)!=2){
            permission.setIcon("layui-icon "+permission.getIcon());
        }
        int updateid = permissionMapper.updateById(permission);
        if (updateid>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public Boolean delPermissionByid(ArrayList<String> list) {
        Boolean flag = false;
        int i = permissionMapper.deleteBatchIds(list);
        if (i>0){
            flag = true;
        }
        return flag;
    }

    @Override
    public List<Integer> selectByridList(Integer rid) {
        if (rid == null){
            return null;
        }
        return permissionMapper.selectByridList(rid);
    }

    @Override
    public Boolean insertRoleToPromission(ArrayList<String> promissionlist, String rid) {
        Boolean flag = false;
        if(rid==null    ||  "".equals(rid)){
            return flag;
        }
        int i = permissionMapper.insertRoleToPromission(promissionlist, rid);
        if (i>0){
            flag  = true;
        }
        return flag;
    }

    public  List<Permission> getAllCommentByBlogId(Integer parentId,Integer uid) {
        List<Permission> commentVos = permissionMapper.getMenuLists(parentId,uid);
        List<Permission> sons = getSons(commentVos,uid);
        return sons;
    }

    private  List<Permission> getSons(Integer parentId,Integer uid){
        return permissionMapper.getMenuLists(parentId,uid);
    }


    private  List<Permission> getSons(List<Permission> parents,Integer uid){
        if(parents == null || parents.size() == 0){
            return null;
        }
        for (Permission parent : parents) {
            int parentId = parent.getId();
            List<Permission> sonCommentVos = getSons(parentId,uid);
            parent.setChildren(getSons(sonCommentVos,uid));
        }
        return parents;
    }
}














