package com.xiaozhi.xzadmin;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.hutool.extra.compress.CompressUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.xiaozhi.xzadmin.mapper.sys.PermissionMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserMapper;
import com.xiaozhi.xzadmin.mapper.sys.UserToRoleMapper;
import com.xiaozhi.xzadmin.pojo.Permission;
import com.xiaozhi.xzadmin.pojo.User;
import com.xiaozhi.xzadmin.service.sys.PermissionService;
import com.xiaozhi.xzadmin.service.sys.RoleService;
import com.xiaozhi.xzadmin.service.sys.UserService;
import com.xiaozhi.xzadmin.tools.excleUtil;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.xiaozhi.xzadmin.tools.StringUtil.findCount;

@SpringBootTest
class XzadminApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private UserToRoleMapper userToRoleMapper;

    @Test
    void contextLoads() {
        User user = new User();
        String string = "aaa456ac";
        user.setUsername("admin");
        user.setPassword("e10adc3949ba59abbe56e057f20f883e");
        user.setNick_name("管理员");
        user.setPhone("13152325621");
        user.setEmail("135434332@qq.com");
        userMapper.insert(user);
        //System.out.println(findCount("aaa456ac", "a"));

    }

    @Test
    void teshhinsert() {
        User user = new User();
        for (int i = 0; i < 10; i++) {
            user.setUsername(setUsername());
            user.setPassword("e10adc3949ba59abbe56e057f20f883e");
            user.setNick_name(setUsername());
            user.setPhone("13152325621");
            user.setEmail("135434332@qq.com");
            userMapper.insert(user);
        }
//        System.out.println(setUsername());
        //System.out.println("写入成功");
    }

    //    随机用户名
    String setUsername() {
        String name = "";
        String model = "abcdefghijklmnopqrstuvwxyz";
        char[] m = model.toCharArray();
        for (int j = 0; j < 6; j++) {
            char c = m[(int) (Math.random() * 26)];
            name += c;
        }

        return name;
    }

    @Test
    void contextLoads1() {
        Permission permission = new Permission();
        permission.setParent_id(0);
        permissionService.setPermissionid(permission);
    }
    @Test
    void deybs() throws ParseException {
        String dateStr = "Mon Apr 04 00:00:00 CST 2022";
        Date date = DateUtil.parse(dateStr);

//        //一天的开始，结果：2017-03-01 00:00:00
//        Date beginOfDay = DateUtil.beginOfDay(date);
//
//        //一天的结束，结果：2017-03-01 23:59:59
//        Date endOfDay = DateUtil.endOfDay(date);

        //System.out.println(date);
        String dateStr1 = "2017-03-01 22:33:23";
        Date date1 = DateUtil.parse(dateStr1);

        String dateStr2 = "2017-02-01 23:33:23";
        Date date2 = DateUtil.parse(dateStr2);

        //相差一个月，31天
        long betweenDay = DateUtil.between(date1, date2, DateUnit.DAY,false);
        //System.out.println(betweenDay);

    }
    @Test
    public void  ase(){
        String content = "1";

//随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

//构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);

        // 加密为16进制表示
        String encryptHex = aes.encryptHex(content);
        // 解密为字符串
        String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        //(encryptHex);
        //System.out.println(decryptStr);
//        System.out.println(encryptionUtils.setEncryptionAes("2"));
    }
    @Test
    public void datasss() throws IOException {
        String path = "D:/home/";
        //当前日期字符串，格式：yyyy-MM-dd
        String today= DateUtil.today();
        excleUtil.creatExcleXlsx(path,today+".xls");

        String filepath = "D:/home/贵阳1/";
        File[] files = FileUtil.ls(filepath);
        for (File file : files) {
            String fileName = file.getName();
            //                文件类型
            String filetype = FilenameUtils.getExtension(fileName).toLowerCase();
            if (filetype.equals("xls")||filetype.equals("xlsx")){
                ExcelReader reader = ExcelUtil.getReader(filepath+fileName);
                List<Map<String,Object>> readAll = reader.readAll();

                ExcelWriter writer = ExcelUtil.getWriter(FileUtil.mkdir(path+today+".xlsx"));
                // 合并单元格后的标题行，使用默认标题样式
//                writer.merge(row1.size() - 1, "一班成绩单");
                // 一次性写出内容，使用默认样式，强制输出标题
                writer.write(readAll);
                // 关闭writer，释放内存
                writer.close();
            }
        }


    }

    @Test
    public void sgbhdsad() throws IOException {
        String path = "D:/home/";
        //1.创建工作簿
        Workbook workbook = new HSSFWorkbook();
        //2.创建表名
        Sheet sheet = workbook.createSheet("2003版表");
        //3.创建行
//        Row row0 = sheet.createRow(0);
//        //4.创建单元格
//        Cell cell = row0.createCell(0);
//        //5.写入数据
//        cell.setCellValue("这是第1,1列的格子");
        //6.创建流用于输出
        FileOutputStream fileOutputStream = new FileOutputStream(path + "2003版本表.xls");
        //7.输出
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        System.out.println("2003版本表已经生成");
    }


    @Test
    public void shnss() throws IOException {
//        合并
        String path = "D:/home/";
        String filepath = "D:/home/贵阳/";
        excleUtil.mergeExcleXlsx(path,filepath);

    }
    @Test
    public void sddsf() throws IOException {
        int numbers = 5;
        String path = "D:/home/";
        //当前日期字符串，格式：yyyy-MM-dd
        String today = DateUtil.today();
        ExcelReader reader = ExcelUtil.getReader(path+today+".xls");
        List<Map<String,Object>> readAll = reader.readAll();
//        System.out.println(readAll.size());
//        System.out.println(readAll.size()/5);

        int number1 = readAll.size() / numbers;
        System.out.println(number1);
        String wiritepath = "D:/home/excle/1/";
        for (int i = 0; i < 5; i++) {
            excleUtil.creatExcleXls(wiritepath,today+"-"+i);
            //通过工具类创建writer
            ExcelWriter writer = ExcelUtil.getWriter(wiritepath+today+"-"+i+".xls");

//            if (i1>readAll.size())
            //一次性写出内容，强制输出标题
            writer.write(readAll.subList(number1*i,number1*(i+1)));
            //关闭writer，释放内存
            writer.close();
        }


    }
    @Test
    public void hbdhbjk() throws IOException {
        String path = "D:/home/";
        String wiritepath = "D:/home/excle/1/";
        excleUtil.splitExcleXls(path,DateUtil.today(),3,wiritepath);
    }
    @Test
    public void  hjakdsg(){
        final File file = FileUtil.file("D:/home/excle/1/test.zip");
        CompressUtil.createArchiver(CharsetUtil.CHARSET_UTF_8, ArchiveStreamFactory.ZIP, file)
                .add(FileUtil.file("D:/home/excle/1"), (file1)->{
                    System.out.println(file1.getName());
                    if("test.zip".equals(file1.getName())){
                        return false;
                    }
                    return true;
                })
                .finish()
                .close();
    }
    @Test
    public void hbdhbjk1() throws IOException {
        String path = "D:/home/excle/1/";
        String zipname = "xizn";
        excleUtil.createZip(path,zipname);
    }

}
