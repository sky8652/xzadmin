#  **xzadmin后台管理系统** 

## 项目简介
xzadmin是一套简单通用的后台管理系统，主要功能有：权限管理、菜单管理、用户管理，系统设置、实时日志，实时监控，API加密，以及登录用户修改密码、配置个性菜单等
## 技术栈
前端：Pear Admin Layui
一个基于 Spring Boot+mybatis-plus+sotaken+Redis+Thymeleaf+hutool+easy-captcha+log4j的后台管理系统
## 文件预览得配合file-online-preview项目(springboot直接食用文件地址（https://gitee.com/kekingcn/file-online-preview/releases或者https://gitee.com/kekingcn/file-online-preview.git）)一起使用  调用下面api
var url = 'http://127.0.0.1:8080/file/test.txt'; //要预览文件的访问地址
window.open('http://127.0.0.1:8012/onlinePreview?url='+encodeURIComponent(base64Encode(url)));
## 常见问题
### 1、IDE编译报错，识别不到实体类的set、get方法？
原因：项目使用lombok开发，lombok会在生成class字节码文件帮我们生成set、get等方法，java文件没有set、get等方法，IDE索引不到set、get方法所以编译报错
解决：IDE安装lombok插件即可能识别到对应set、get方法，重启生效
### 2、默认用户名密码
后台地址：http://127.0.0.1:8080/admin/login
admin 123456超级管理员
xiaozhi 123456管理员
xiaoou 123456用户
